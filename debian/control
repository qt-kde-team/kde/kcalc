Source: kcalc
Section: math
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Sune Vuorela <sune@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-kf6,
               cmake (>= 3.16~),
               extra-cmake-modules (>= 6.0.0~),
               gettext,
               libgmp-dev,
               libkf6colorscheme-dev (>= 6.0.0~),
               libkf6config-dev (>= 6.0.0~),
               libkf6configwidgets-dev (>= 6.0.0~),
               libkf6coreaddons-dev (>= 6.0.0~),
               libkf6crash-dev (>= 6.0.0~),
               libkf6doctools-dev (>= 6.0.0~),
               libkf6guiaddons-dev (>= 6.0.0~),
               libkf6i18n-dev (>= 6.0.0~),
               libkf6notifications-dev (>= 6.0.0~),
               libkf6xmlgui-dev (>= 6.0.0~),
               libmpfr-dev,
               libxkbcommon-dev,
               qt6-base-dev (>= 6.6.0+dfsg~),
Standards-Version: 4.7.0
Homepage: https://apps.kde.org/en/kcalc
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kcalc
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kcalc.git
Rules-Requires-Root: no

Package: kcalc
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: simple and scientific calculator
 KCalc is a scientific calculator.
 .
 KCalc provides:
  * Trigonometric functions, logic operations, and statistical calculations
  * Calculation in decimal, hexadecimal, octal, and binary bases
  * Memory functions for storing results
  * A comprehensive collection of mathematical constants
 .
 This package is part of the KDE Utilities module.
